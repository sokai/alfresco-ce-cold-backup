#!/bin/bash
# -------------------------------------------------------------- #
# Script for Alfresco CE (stand alone version) cold backup       #
# Origin: http://forums.alfresco.com/fr/viewtopic.php?f=8&t=3921 #
# -------------------------------------------------------------- #
# v1.9
# 2011-08-04
#
# --- HowTo
# 1. put this executable script "alfresco_ce-cold_backup.sh" into /root/bin
# 2. create the folder /home/backup (OR change $BACKUP_ROOT)
# 3. set $SOW (start of week)
# 4. set $DO_RSYNC if you want to rsync your backup directory (AND change the variables in myrsync() )
# 5. add the following line into the crontab of user root ("crontab -e" as root)
#    0 1 * * * /root/bin/alfresco_ce-cold_backup.sh /opt/alfresco-3.4.d 0>&- 1>&- 2>&-
# ---

# exit function
function exitBackup() {
	if [ $1 == "E" ]; then
		echo " Error:"
	fi
	if [ $1 == "S" ]; then
		echo " Success:"
	fi
	echo -e "   ${2}"
	echo
        exit 1
}

check_dir() {
	[ ! -e $1 ] || {
		exitBackup "E" "directory '${1}' still exists."
	}
}

# This works by starting `tee` in the background with its stdin
# coming from a named pipe that we make; then we redirect our
# stdout and stderr to the named pipe. All pipe cleanup is handled
# in a trap at exit.
# Origin: http://www.noah.org/wiki/Bash_notes
# This is the exit trap handler for the 'tee' logger.
on_exit_trap_cleanup() {
	# Close stdin and stdout which closes our end of the pipe
	# and tells `tee` we are done.
	exec 1>&- 2>&-
	# Wait for `tee` process to finish. If we exited here then the `tee`
	# process might get killed before it hand finished flushing its buffers
	# to the logfile.
	wait $TEEPID
	/bin/rm ${PIPEFILE}
}
tee_log_output() {
	#LOGFILE=$1
	#PIPEFILE=$(mktemp -u ${TMP_DIR}/${SUFFIX}-pipeXXX)
	#PIPEFILE=$(mktemp -u /tmp/$(basename $0)-pid$$-pipe-XXX)
	PIPEFILE=$(mktemp -u ${TMP_ROOT}/$(basename $0)-pid$$-pipe-XXX)
	mkfifo ${PIPEFILE}
	tee ${LOGFILE} < ${PIPEFILE} &
	TEEPID=$!
	# Redirect subsequent stdout and stderr output to named pipe.
	exec > ${PIPEFILE} 2>&1
	trap on_exit_trap_cleanup EXIT
}

SOW=1 # start of week; 1=Monday; 7=Sunday
DO_CLEAN_LOGS=1
DO_CLEAN_OLD_BACKUPS=0
DO_RSYNC=1
RSYNC_SRV="fhpalf" # set if DO_RSYNC=1

DATE=$(date +%y%m%d_%H%M)
PID=$$
MACHINE=$(hostname -f)
SUFFIX="alf_backup"
BACKUP_ROOT=/home/backup
BACKUP_DIR=${BACKUP_ROOT}/${MACHINE}/${DATE}
TMP_ROOT=/tmp
TMP_DIR=${TMP_ROOT}/${DATE}-${SUFFIX}-pid${PID}
TMP_DIR_FILES=${TMP_DIR}/files
LOGFILE=${BACKUP_DIR}/${DATE}-${MACHINE}-${SUFFIX}.log

check_dir $TMP_DIR && mkdir $TMP_DIR
check_dir $BACKUP_DIR && mkdir $BACKUP_DIR
tee_log_output $LOGFILE

# At this line all script output goes to stdout _and_ the $LOGFILE

# extract defined Alfresco config values from  Alfresco properties file
# Parameter: $1=NAME, $2=VALUE
get_alfdata() {
	eval ${1}=\"$(sed "/^\#/d" $ALF_CONFIG | grep "^"${2}  | tail -n 1 | cut -d "=" -f2-)\"
}

#
# Configuration
###
# -- Alfresco
#ALF_ROOT=/opt/alfresco-3.4.c
ALF_ROOT=$1
ALF_CONFIG=${ALF_ROOT}/tomcat/shared/classes/alfresco-global.properties
ALF_EXT_DIR=${ALF_ROOT}/tomcat/webapps/alfresco/WEB-INF/classes/alfresco/extension
get_alfdata "ALF_DATA" "dir.root"

# --- MySQL
MYSQL_CONF="${TMP_DIR}/my.cnf"
MYSQL_HOST="localhost"
MYSQL_PORT=3306
MYSQL_SOCKET=${ALF_ROOT}/mysql/tmp/mysql.sock
MYSQL_CSET="UTF8"
MYSQL_SUFFIX="_alfresco"
MYSQL_OPTIONS="--defaults-file=${MYSQL_CONF} --defaults-group-suffix=${MYSQL_SUFFIX} --allow-keywords --comments --complete-insert --compress --debug-check --force --max_allowed_packet=50M --opt --routines --triggers --single-transaction"
#MYSQL_OPTIONS="${MYSQL_OPTIONS} --verbose"

# --- Backup
BACKUP_FILE=${BACKUP_DIR}/${DATE}-${MACHINE}-${SUFFIX}.tar.gz

# Functions
###
# make sure we're running as root
function chk_root() {
	[ $(id -u) == 0 ] || {
		exitBackup "E" "Sorry, you have to be the super user (root) to execute this script.\n"
	}
}

# control services (start/stop/restart/status)
function service_ctl() {
	echo
	echo "### Alfresco service(s) control"
	[ ! -z $2 ] && echo "### ${2}: ${1}"
	[ -z $2 ] && echo "### ${1}"
	echo "# ----------"
	(
		#/etc/init.d/alfresco $1 tomcat/mysql
		#service alfresco status/start/stop tomcat/mysql
		service alfresco $1 $2
		wait
	)
}

# helper function for killing a PID (ie soffice.bin)
function pkill(){
	echo
	echo "### killall: ${1}"
	echo "# ----------"
	for i in `ps ax | grep ${1}| cut -d ' ' -f 1`
	do
		echo " i kill -9 $i"
		kill -9 $i
		echo " success"
	done
	echo " done"
}

function mykillall(){
	echo
	echo "### killall: ${1}"
	echo "# ----------"
	(
		/usr/bin/killall -v $1
		wait
	)
	echo " done"
}

# backup MySQL database
function backup_mysql() {
	# generate temp. my.cnf
	echo
	echo "### MySQL backup"
	echo "# ----------"
	touch $MYSQL_CONF
	if [ ! -f $MYSQL_CONF ]; then
		exitBackup "E" "MySQL config file not found. Aborting backup!"
	else
		get_alfdata "DB_USERNAME" "db.username"
		get_alfdata "DB_PASSWORD" "db.password"
		get_alfdata "DB_NAME" "db.name"
		echo "[client${MYSQL_SUFFIX}]" > $MYSQL_CONF
		echo "host=\"${MYSQL_HOST}\"" >> $MYSQL_CONF
		echo "port=\"${MYSQL_PORT}\"" >> $MYSQL_CONF
		echo "socket=\"${MYSQL_SOCKET}\"" >> $MYSQL_CONF
		echo "default-character-set=\"${MYSQL_CSET}\"" >> $MYSQL_CONF
		#echo "database=\"${DB_NAME}\"" >> $MYSQL_CONF # database has to be in the cli command!
		echo "user=\"${DB_USERNAME}\"" >> $MYSQL_CONF
		echo "password=\"${DB_PASSWORD}\"" >> $MYSQL_CONF
	fi
	# backup database
	(
		${ALF_ROOT}/mysql/bin/mysqldump.bin $MYSQL_OPTIONS $DB_NAME > ${TMP_DIR_FILES}/${DB_NAME}.sql
		wait
	)
	echo " done"
}

function backup_alfdata() {
	echo
	echo "### ALF_DATA backup"
	echo "# ----------"
	(
		cd $TMP_DIR_FILES
		/bin/cp -R $ALF_DATA .
		wait
	)
	echo " done"
}

function backup_extensions() {
	echo
	echo "### ALF_EXTENSIONS backup"
	echo "# ----------"
	(
		cd $TMP_DIR_FILES
		/bin/cp -vR $ALF_EXT_DIR .
		wait
	)
	echo " done"
}

function backup_config() {
	echo
	echo "### ALF_CONFIG backup"
	echo "# ----------"
	(
		cd $TMP_DIR_FILES
		/bin/cp -v $ALF_CONFIG .
		wait
	)
	echo " done"
}

function backup_tomcatlogs() {
	TOMCATLOGS_BDIR="logs_tomcat"
	LOG_DATE=$(date -d "-1 day" +%Y-%m-%d)
	echo
	echo "### ALF_TOMCATLOGS backup"
	echo "# ----------"
	(
		cd $TMP_DIR_FILES
		mkdir $TOMCATLOGS_BDIR
		[ -e ${ALF_ROOT}/tomcat/logs/catalina.out ] && /bin/cp -v ${ALF_ROOT}/tomcat/logs/catalina.out ${TOMCATLOGS_BDIR}/
		[ -e ${ALF_ROOT}/tomcat/logs/${ALF_ROOT}/tomcat/logs/host-manager.${LOG_DATE}.log ] && /bin/cp -v ${ALF_ROOT}/tomcat/logs/host-manager.${LOG_DATE}.log ${TOMCATLOGS_BDIR}/
		[ -e ${ALF_ROOT}/tomcat/logs/${ALF_ROOT}/tomcat/logs/localhost.${LOG_DATE}.log ] && /bin/cp -v ${ALF_ROOT}/tomcat/logs/localhost.${LOG_DATE}.log ${TOMCATLOGS_BDIR}/
		[ -e ${ALF_ROOT}/tomcat/logs/${ALF_ROOT}/tomcat/logs/manager.${LOG_DATE}.log ] && /bin/cp -v ${ALF_ROOT}/tomcat/logs/manager.${LOG_DATE}.log ${TOMCATLOGS_BDIR}/
		wait
	)
	echo " done"
}

function backup_mysqllogs() {
	MYSQLLOGS_BDIR="logs_mysql"
	echo
	echo "### ALF_MYSQLLOGS backup"
	echo "# ----------"
	(
		cd $TMP_DIR_FILES
		mkdir $MYSQLLOGS_BDIR
		[ -e ${ALF_ROOT}/mysql/data/mysqld.log ] && /bin/cp -v ${ALF_ROOT}/mysql/data/mysqld.log ${MYSQLLOGS_BDIR}/
		wait
	)
	echo " done"
}

function backup_alflogs() {
	ALFLOGS_BDIR="logs_alfresco"
	echo
	echo "### ALF_ALFRESCOLOGS backup"
	echo "# ----------"
	(
		cd $TMP_DIR_FILES
		mkdir $ALFLOGS_BDIR
		[ -e ${ALF_ROOT}/alfresco.log ] && /bin/cp -v ${ALF_ROOT}/alfresco.log ${ALFLOGS_BDIR}/
		[ -e ${ALF_ROOT}/webquickstart.log ] && /bin/cp -v ${ALF_ROOT}/webquickstart.log ${ALFLOGS_BDIR}/
		wait
	)
	echo " done"
}

function create_backupfile() {
	echo
	echo "### creating backup file"
	echo "# ----------"
	(
		if [ -d $BACKUP_DIR ]; then
			cd $TMP_DIR_FILES
			tar -czf $BACKUP_FILE *
			wait
		fi
	)
	echo " done"
}

function symlinks() {
	cd ${BACKUP_ROOT}/${MACHINE}
	(
		echo
		echo "### creating new daily symlink"
		echo "# ----------"
		if [ -d ${DATE} ]; then
			# http://blog.moertel.com/articles/2005/08/22/how-to-change-symlinks-atomically
			ln -s ${DATE} "last-day_tmp" && mv -Tf "last-day_tmp" "last-day"
			echo " done"
		else
			echo " Todays backup doesn't exists. Skipping link creation."
		fi
	)
	(
		# new week
		WEEK_DAY=$(date +%u)
		if [ $WEEK_DAY == ${SOW} ]; then
			echo
			echo "### creating new weekly symlink"
			echo "# ----------"
			LAST_WEEK=$(date -d "-7 days" +%y%m%d)
			LAST_WEEK_DIR=$(find . -name "${LAST_WEEK}*" -type d|sort -r|head -1)
			LAST_WEEK_DIR=${LAST_WEEK_DIR#./}
			if [ "${LAST_WEEK_DIR}" != "" ] && [ -d ${LAST_WEEK_DIR} ] ; then
				ln -s ${LAST_WEEK_DIR} "last-week_tmp" && mv -Tf "last-week_tmp" "last-week"
				echo " done"
			else
				echo " Backup of the last week doesn't exists. Skipping link creation."
			fi
		fi
		
	)
	(
		# last day of month
		LASTDAY_THISMONTH=$(date -d "+1 month -$(date +%d) days" +%y%m%d)
		#FIRSTDAY_THISMONTH=$(date -d "-$(($(date +%d)-1)) days" +%y%m%d)
#		if [ $(date +%y%m%d) == ${LASTDAY_THISMONTH} ]; then
			echo
			echo "### creating new monthly symlink"
			echo "# ----------"
			LAST_MONTH=$(date -d "-$(date +%d) days" +%y%m%d)
			LAST_MONTH_DIR=$(find . -name "${LAST_MONTH}*" -type d|sort -r|head -1)
			LAST_MONTH_DIR=${LAST_MONTH_DIR#./}
			if [ "${LAST_MONTH_DIR}" != "" ] && [ -d ${LAST_MONTH_DIR} ] ; then
				ln -s ${LAST_MONTH_DIR} "last-month_tmp" && mv -Tf "last-month_tmp" "last-month"
				echo " done"
			else
				echo " Backup of the last month doesn't exists. Skipping link creation."
			fi
#		fi
	)
	echo
	echo "### backup directories"
	echo "# ----------"
	echo " last-day  : "$(readlink "last-day")
	echo " last-week : "$(readlink "last-week")
	echo " last-month: "$(readlink "last-month")
}

function clean_logs() {
	if [ $DO_CLEAN_LOGS == 1 ]; then
		echo
		echo "### clean tomcat log files"
		echo "# ----------"
		(
			/bin/rm -vf ${ALF_ROOT}/tomcat/logs/*
			wait
			echo " done"
		)
		echo
		echo "### clean alfresco log files"
		echo "# ----------"
		(
			[ -e ${ALF_ROOT}/alfresco.log ] && /bin/rm -vf ${ALF_ROOT}/alfresco.log*
			[ -e ${ALF_ROOT}/webquickstart.log ] && /bin/rm -vf ${ALF_ROOT}/webquickstart.log*
			echo "done"
		)
	fi
}

function clean_old_backups() {
### ToDo
	if [ $DO_CLEAN_OLD_BACKUPS == 1 ]; then
		echo
	fi
}

function myrsync() {
	if [ $DO_RSYNC == 1 ]; then
		echo
		echo "### rsync backup directory"
		echo "# ----------"
		(
			RSYNC_DIR="${BACKUP_ROOT}/"
			RSYNC_MOD="alf_backup"
			/usr/bin/rsync -avz ${RSYNC_DIR} ${RSYNC_SRV}::${RSYNC_MOD}
			wait
		)
		echo " done"
	fi
}

function cleanup() {
	echo
	echo "### cleanup"
	echo "# ----------"
	(
		cd ${TMP_ROOT}
		/bin/rm -rf ${TMP_DIR}
		wait
	)
	echo " done"
	
}

echo "### Alfresco backup"
echo "# ----------"
cd $ALF_ROOT
chk_root
check_dir $TMP_DIR_FILES && mkdir $TMP_DIR_FILES
service_ctl status
service_ctl stop tomcat
#service_ctl stop
#service_ctl start mysql
##pkill soffice.bin
#mykillall soffice.bin
backup_mysql
backup_alfdata
backup_extensions
backup_config
backup_tomcatlogs
backup_mysqllogs
backup_alflogs
create_backupfile
symlinks
clean_logs
clean_old_backups
myrsync
cleanup
service_ctl start tomcat
#service_ctl status # disabled because the script hangs & PID file will not become deleted

exit 0
